import React, { useEffect, useState } from 'react';
import ResearchCardGrid from '../ResearchCardGrid';
import Footer from '../Footer';
import Content from '../Content';
import './OngoingProjects.css';
import { Link } from 'react-router-dom';
import yaml from 'js-yaml'; // Import js-yaml library

function OngoingProjects() {
  const [projects, setProjects] = useState([]);
  const [filteredProjects, setFilteredProjects] = useState([]);
  const [activeCategory, setActiveCategory] = useState(null);

  // Category-specific images
  const categoryImages = {
    'City Research + CoLab': '/images/city_research.png',
    'AI/ML Digital Twin Efforts': '/images/AI_ML_cover.png',
    'Hurricanes and Extreme Weather': '/images/hurricane1.png',
    'International Partnerships': '/images/international_partnership.png',
  };

  useEffect(() => {
    // Specify the path to your YAML file
    const yamlFilePath = process.env.PUBLIC_URL + '/Projects.yaml';

    // Use fetch to load the YAML file
    fetch(yamlFilePath)
      .then((response) => response.text())
      .then((yamlData) => {
        // Parse the YAML data into an array of objects
        const parsedProjects = yaml.load(yamlData);
        setProjects(parsedProjects);
        setFilteredProjects(parsedProjects); // Initially show all projects
      })
      .catch((error) => {
        console.error('Error fetching or parsing YAML file:', error);
      });
  }, []);

  const handleCategoryClick = (category) => {
    setActiveCategory(category);

    const filtered = projects.filter((project) => project.Category === category);
    setFilteredProjects(filtered);
  };

  const handleBackToCategories = () => {
    setActiveCategory(null); // Reset and show categories again
    setFilteredProjects(projects); // Show all projects again when back to categories
  };

  return (
    <>
    <div style={{ background: "#f7f7f7" }}>
        <div className="header-banner">
          <div className="wall-container">
            <video className="bg-video" autoPlay muted loop playsInline>
              <source src={process.env.PUBLIC_URL + '/videos/drone-video.mp4'} />
            </video>
          </div>
          <h1>Ongoing Projects</h1>
        </div>

       
        {/* Display Category Boxes if no category is selected */}
        {activeCategory === null ? (
          <div className="category-boxes">
            {['City Research + CoLab', 'AI/ML Digital Twin Efforts', 'Hurricanes and Extreme Weather', 'International Partnerships'].map((category) => (
              <div
                key={category}
                className={`category-box ${activeCategory === category ? 'active' : ''}`}
                onClick={() => handleCategoryClick(category)}
                style={{ backgroundImage: `url(${process.env.PUBLIC_URL + categoryImages[category]})` }} // Use specific image for each category
              >
                <div className="category-overlay">
                  <h2>{category}</h2>
                </div>
              </div>
            ))}
          </div>
        ) : (
          // Show filtered projects if a category is selected
          <>
            <button onClick={handleBackToCategories} className="back-button button-style" style={{}}><i className="fas fa-arrow-left"></i> Back to Categories</button>
            <div className="cardsgrid">
              <ResearchCardGrid cards={filteredProjects} />
            </div>
          </>
        )}
      </div>
    {/* <div style={{background:"#f7f7f7"}}>
    <div className="header-banner">
    <div className="wall-container">
        <video className="bg-video" autoPlay muted loop  playsInline>
          <source src={process.env.PUBLIC_URL + '/videos/drone-video.mp4'} />
        </video>
        </div>
         <h1>Ongoing Projects</h1>
    </div>
      <div className="cardsgrid">
        <ResearchCardGrid cards={projects} />
      </div>
      </div> */}

       {/* 
    This is a multi-line comment.
    <p>You can comment out elements like this, too.</p>
  

      <div className="ongoing-projects-page">
        {projects.map((project, index) => (
          <div key={index}>
            <Content
              title={project.title}
              pictureSrc={project.pictureSrc}
              content={project.longContent}
              targetSection={project.targetSection}
            />
            <Link to={`/content/${project.targetSection}`}>Read More</Link>
          </div>
        ))}
      </div>
      */}

      <Footer />
    </>
  );
}

export default OngoingProjects;