
const NewsInfo = [
  {
    title: "Dr. Atul Jain, Professor, University of Illinois visits UT Austin",
    date: "October 5th",
    image: "/images/atul.jpeg", // Replace with the actual image URL or import statement
    link: "/news/article1", // Replace with the actual route you want to link to
  },
  {
    title: "Urban Climate Solutions Workshop On the campus of Texas A&M University",
    date: "October 9th & 10th 2023",
    content: "The TExUS lab took a trip to attend the Urban Climate Solutions workshop where Dev Niyogi and Harsh Kamath presented their research and served on panels during the workshop.",
    image: "images/workshop.jpeg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
    title: "2023 Solar Eclipse",
    date: "October 14th 2023",
    content: "The Oct 14 eclipse in Texas backyard -- and an even more dramatic one coming in April 2024 -- provides an amazing opportunity for geoscience education, and understanding the earth system feedbacks. So the UT Extreme weather/climate and Urban Sustainability (TExUS) lab partnered with community groups led by Amol Manduskar (CSM) and a team of about 60 citizen scientists, enthusiasts who travelled 2.5 hours south of Austin to chase the eclipse and its totality. Finding the location with two tropical disturbances offshore earlier in the week, the transition of wind regimes, reviewing the weather forecasts and developing discussions with the team and providing the input to the community was a fun challenge. Even when we reached Bourne, TX there were clouds and the group was getting concerned about viewing conditions. We then had to pull out different satellite imagery, nowcasts, and help find a spot where a cloud free sighting could be experienced. This was bringing the geoscience education to the community especially the middle schoolers and youth that were present-- and sharing the fascinating science in its different facets. This eclipse event comes when our department has been recently named as #EarthPlanetarySciences within the #JacksonSchoolofGeosciences -- and what better way than to see the relation between the Earth and our Planetary System than the Eclipse?! I express kudos to our group (their ownership of such events and activities, the passion with which they do these things, traveling on a weekend, trying to catch a worldcup cricket match on cell phone while driving down, staying cheerful and sharing ideas and working with the community is just endearing and energy pumping!)- and a big thank you to the citizen science groups especially Amol Manduskar (CSM) for his amazing coordination and interfacing with the teams. We could get some fantastic view of the annular solar eclipse with 'Ring of Fire', photography of eclipse and ring of fire, capture the shadow bands and discuss diffraction, surface and air and infra radiation temperature changes in wake of radiation changes. Indeed such events also allow some introspective philosophical insights. Especially the vastness of the universe- and how we are just one tiny minuscule component of that whole part (let alone our day to day aspirations and ups and downs that we let overcome the experience we have in this infinitesimal flash of moment in the galaxy we call our life). Hope we can use such events to cherish where we are and what we do. Thank you everyone for making this life beautiful.and Thank you team for making science and discovery process fun!",
    image: "images/eclipse.jpeg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },
  {
   title: "Harsh Completes Quals!",
    date: "November 9th 2023",
    content: "Our PHD student Harsh Kamath has successfully completed his qualifying exam! Congratulations!",
    image: "images/HarshQuals.jpeg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "Dr. Atul Jain visits us again to speak DeFord Lecture Series",
    date: "November 9th 2023",
    content: "We were privileged to host Dr. Atul Jain of The University of Illinois Urbana-Champaign. He is elected a Fellow of the American Association for the Advancement of Science (AAAS) and the American Geophysical Union (AGU). Dr. Jain discusssed the potential and risks of using Bioethanol as an energy source. ",
    image: "images/AtulVisi2.jpeg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "End of Year Dinner & Congratulations Dev!",
    date: "December 5th 2023",
    content: "Wrapping up for the year, we had our end of semester dinner and celebrated Dev being nominated as a Fellow of the Indian Meteorological Society! Wishing everyone happy holidays and a happy new year! See you next year.",
    image: "images/GroupDinner.jpeg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },


  {
   title: "Dev, Trevor and Harsh in Paris! ",
    date: "December 12th 2023",
    content: "Dev, Trevor and Harsh participate WWRP Summer Olympics and Paralympics Research Demonstration Project Meeting in Paris involving different operational  and academic research partners, looking into the aspect of urban heat waves and urban thunderstorms.",
    image: "images/paris.jpg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "New Class: GeoHealth ",
    date: "January 16th 2024",
    content: "Announcing a new class offered starting Spring 2024, GeoHealth. This course aims to examine the health impacts that result from climate change and environmental degradation and how the health world has aimed to respond to these changes.",
    image: "images/geohealth.png", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "The Group Attends AMS",
    date: "January 28th 2024",
    content: "American Meteorological Society Annual Meeting was everything it promised and some more! The buzz and the numerous conversations and meetings that went on through the convention center, the hotel lobbies, and the restaurants near by highlighted the prominent and societally pivotal role AMS and meteorological sciences has in today's world. The buzz around extreme weather, the NOAA Hurricane and extreme weather predictions, NCAR community partnerships, the many large initiatives such as DOE IFLs and ARM program, the serious intent across the community to bridge science with applications, weather with climate, data with knowledge, AI with ethics, and AMS with hope and future...yields a vibrancy that our group and the broader community will find itself propelling into newer topical areas, discoveries and dimensions. Cant wait to see what we will be talking about and sharing in NOLA next year for AMS2025!",
    image: "images/ams.jpg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "First in Person CoLab Workshop!",
    date: "February 8th 2024",
    content: "The first in-person workshop for the UT - City Climate CoLab kicks off!",
    image: "images/colab11.jpg", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
    title: "JSG Poster Symposium for Graduate and Undergraduates",
    date: "February 16th 2024",
    content: "The JacksonSchoolGeosciences annual research symposium was a nice avenue for students - current graduate and undergraduates, - even highschool interns, and prospective/ visiting students to meet and learn about the program and the student experience / work underway. The symposium has become covetted with active faculty participation, and the industry as well as national lab support (e.g. Shell, Chevron, ExxonMobil, ConocoPhillips and Sandia National Laboratories) which provides notable cash awards for best presentations - which has been another important draw for many students to put in their best. This year we had members of the TExUS lab showcase their work from urban rainfall changes, to use of language models for heat advisories, to data fusion products and models, the Austin ThermalScape where we have generated 2m spatial resolution temperature data for the entire city and have visualization overlays, and also the brown ocean/ soil moisture feedback. Such symposia really add to highlighting the spirit of what academia truly is- inspiring, educational, about mentoring, team work, bold, diverse, celebratory, dare to dream attitude builder, and above all supportive, brilliant community. It highlights why some of us, decades after graduating, continue and in the first place aspired to stay back and become academic. Seeing the ideas flourish, dreams emerge, futures written all around with young enthusiastic, energetic students is electric (and renewable!) Kudos to all and wishes to everyone to stay hungry for loving and living life and all that it offers. Happy Sunday! PS: One of the lab members did win a nice prize  and in the spirit of 'one-for-all' the festivities, I understand, are getting planned! Congratulations. JSG GSEC (Nicole Czwakiel, Mikayla Pascual), @Melissa Armstrong, Kristen Tucek,Claudia Mora Danny Stockli Anton Caputo Michael Young Courtney Vletas",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60",
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "Alka Tiwari Visiting from Purdue",
    date: "April 3rd",
    content: "Alka is visiting UT Austin on a research grant from Purdue University, where she got her PhD in Civil Engineering. She got her Masters of Technology in Civil Engineering at the Indian Institute of Technology in Kanpur and her Bachelor of Technology in Civil Engineering at KNIT Sultanpur. Alka is interested in understanding and providing tools or solutions for multiscale hydroclimatic issues. She is proficient in R, Python, and Matlab amongst many other skills and has interests in hurricanes, satellite data, rainfall, and hydrology.",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "Jagadish Shukla from George Mason University!",
    date: "April 10th 2024",
    content:' "Who is Charney?", "How many of you know von Neumann and Ed Lorenz, and what they did"? "What is predictability"? to "How do you formulate a research problem, and start your research"? "Should you trust a model"? These and more were the questions that got the students and the group pumped up with the visit of Jagadish Shukla from George Mason University and COLA. It was a treat to have Prof. Shukla spend an entire day interacting with the students, and the Jackson School of Geosciences at The University of Texas at Austin climate community, discussing how the new climate science curriculum can be built further, the role of models and observations and partnerships in building climate initiatives. It was fascinating to hear of his first meeting with Charney and Manabe and the topics of convection, as well as anecdotes like what we know of Lorenz butterfly effect could have been called a seagull effect of Texas and was changed to butterfly effect by the host, and see the discussions build around the role of physics-based and AI/ML modeling approaches for improving climate predictions. Limits of predictability, and the topic of my interest - cities and climate, as well as how do we not stop asking the questions - why did the model behave the way it did - why did it give the right result - should it have given that type of result -- what would be a better or worse model- should we have more models or lesser models? It was wonderful to see the students engage and discuss ideas and Prof. Shukla spend evenings in a casual setting at a food truck park to complete the Austin experience! We even managed a hail storm, and rain burst to just make things more fun!',
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60",
    link: "/news/article2", // Replace with the actual route you want to link to
  },

   {
   title: "Manmeet Singh Joined the Team in Person!",
    date: "INSERT  2024",
    content: "Manmeet is a climate data scientist with experience in Earth System Modelling, nonlinear dynamics, deep learning applications, complex networks, and interested in anything AI. He is working towards a sustainable future for all on the planet Earth. Manmeet got his Bachelor of Engineering and Civil Engineering from the Thapar Institute of Engineering & Technology, his PhD in Climate Studies from the Indian Institute of Technology in Bombay, and is now at UT Austin for his postdoc!",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60",
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
   title: "CoLab Ideas Presented to the City’s Joint Sustainability Council!",
    date: "April 26th 2024",
    content: "We had the good opportunity to present the #UTCityClimateCoLab ideas, progress, and plans to the City's Joint Sustainability Council and seek their ideas and advice. Our CoLab team highlighted the uniqueness of the partnership taking a co production approach for tools, and datasets that are aligned with city departments and community needs and priorities. This integrative framework, I am getting more and more convinced, is critically needed by all cities as we take on the topics of climate smart investments, carbon neutral goals, and investments within the community and city services with a long view. We have conversations with #UNESCO about building further on this and would be a good template for the #IPCC City Climate Special Report to consider. Topics such as, how do we prioritize projects we take on, how to sustain support and the network through local and federal funding, how to develop impactful educational experiences and research projects for students, and shorten the research ideas to community impact timeline, came up in the discussions. Excited and grateful for this amazing team and everyone's support to the #CityClimateCoLab!",
    image: "https://media.licdn.com/dms/image/D5622AQGi3KqFdCnTRg/feedshare-shrink_2048_1536/0/1714049697371?e=1718841600&v=beta&t=lBb5q6RFtGLR-2T5Lx_oUjLhofWW0yvnrn77TJF6rRo", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },


  {
   title: "Welcome Alexia Leclercq as our new Program Manager!",
    date: "May 8th 2024",
    content: "Alexia (she/they) is our newest Program Manager! She is also a grassroots organizer, scholar, and artist. She is the co-founder of the Colorado River Conservancy and a political education non-profit named Start: Empowerment. Her curriculum has reached over 120,000 students across the United States and her work has been recognized by NYC Department of Education, Bloomberg, Forbes, Washington Post, Yahoo, Guardian, Austin Statesman, and more. She has also won the prestigious Brower Youth Award, Jericho Activism Prize, and the 2022 WWF Conservation Award. Alexia served as the 2022 UN Youth Assembly Ambassador and has also been invited to speak at various events such as COP27, Harvard, Global Peace Education Conference, Bioneers conference, CUNY Climate Education Conference, and Princeton University, etc. Alexia graduated Summa Cum Laude from New York University and Harvard University. Her research interests include: political economy, inequality, (Post)colonial studies, liberation pedagogy, Queer, Asian & Indigenous ecologies, and environmental justice. Her art, which explores themes of immigration, mental health, community, and justice has been featured in the People's Gallery at Austin City Hall, and at the Gallatin Arts Festival. Alexia aims to build a new world centered around liberation, joy, healing, and justice",
    image: "https://media.licdn.com/dms/image/D4E22AQHBiHHQy_atsQ/feedshare-shrink_1280/0/1685298997730?e=1718841600&v=beta&t=_rlsgqdDLgeVcVyFrricALeKlZopE8LJtG6y8U7YJuE", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },


  {
    title: "UT Osher Lifelong Learning Institute (OLLI)",
    date: "May 9th 2024",
    content: "",
    image: "https://media.licdn.com/dms/image/D5622AQG0PjBbA1nCWg/feedshare-shrink_2048_1536/0/1715213159678?e=1718841600&v=beta&t=DdUWJQxbccTM279ysFgbyV58woRxaHnvtI10_TVE8eE", 
    link: "/news/article2", // Replace with the actual route you want to link to
  },


  {
   title: "Welcome Alex, our REU Student!",
    date: "May 30th 2024",
    content: "Alex just joined the UT-City Climate CoLab as an REU Student! She is a third-year student at Johns Hopkins University pursuing a Bachelor of Science degree in Environmental Science with a minor in Writing Seminars. She is seeking opportunities with organizations focused on environmental conservation, sustainable development, and environmental policy and governance. Her research, here at UT Austin, will focus on analyzing urban heat islands in the Austin area using ArcGIS mapping software. She is excited to gain hands-on research experience and work with talented UT faculty!",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },



  {
   title: "CRESSLE Postdoctoral Koorosh Joins the Team!",
    date: "June 4th 2024",
    content: "Koorosh just started his Postdoctoral Fellowship at UT Austin and is the newest addition to the CoLab! His research spans various interdisciplinary aspects of water and environmental systems, including climate risk assessment, decision-making processes, coupled human-natural systems, policy analysis, sustainability, and mitigation and adaptation strategies. He is focused on understanding the dynamic interactions between various components of socio-environmental systems, especially in the context of urban water management. His work involves investigating the impacts of diverse climatic risks on urban water systems and communities, advancing the theory of coupled systems, translating this theory into actionable models, and applying these models to support infrastructure planning and policy analysis. His aim is to foster more sustainable and resilient practices in response to future climate risk challenges, with a high emphasis on equity and justice.",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60",// Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },


 {
   title: "Welcome Krishna!",
    date: "September 2023",
    content: "Krishna is a researcher/scientist working in Atmospheric Physics focusing on the following areas in the National Centre for Earth Science Studies (NCESS): Tropical Extreme Weather, Monsoon, Rainfall, Lightning and thunderstorms, Heat waves, Land-atmosphere coupling, & related Natural hazards. He started his research at the Space Physics Laboratory (SPL) in the Indian Space Research Organization (ISRO) as an M.Tech internship student working on satellite-based land surface microwave emissivity retrieval with senior scientist Dr. C .Suresh Raju. Then, he received a prestigious ISRO research fellowship and joined the PhD program at the National Atmospheric Research Laboratory (NARL), ISRO, Andra Pradesh, India. He investigated land-atmosphere coupling over South Asia under Dr. M. Rajeevan (Former Secretary, MoES, Govt of India). WRF regional and NOAH land surface models were used to study and develop new offline soil moisture data. He was also actively involved in NARL research & development activities, including severe weather NWP forecasting support for ISRO satellite launching and monthly atmospheric radar operations. At the National Centre for Medium-Range Weather Forecasting (NCMRWF, India), he worked as a project scientist/post-doctoral fellow on land surface modeling and the UK-Metoffice model. He coordinated the incorporation of ISRO land surface satellite data in the UK-Met Office model in collaboration with NRSC ISRO and the UK Met Office. He also worked with the JULES land surface model to improve Unified Model surface fluxes. He joined the University of Texas (UT) in Austin, United States, as an exchange researcher/scientist in 2023. I work with Prof. Zong-Liang Yang and Prof. Dev Niyogi , Department of Earth and Planetary Sciences on studying tropical heat wave natural hazards & land surface interactions as part of the SERB SIRE program.",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },


  {
   title: "Heat Press Conference and KXAN",
    date: "June 4th 2024",
    content: "The City of Austin held a press conference Monday morning to share important health and safety information during excessive heat conditions. The CoLab produced projections, the seasonal outlook, and playbook so the community could have access to readily available information on how to stay cool during this summer! The projections include the Austin Future Climate Report, which contains information about the increasing trend of higher summer temperatures, heatwaves, and what we should expect for the precipitation levels and amount of cold nights. These outcomes were analyzed using two different scenarios, the “high-emissions scenario” and the “business-as-usual emissions scenario”. The Austin Summer Outlook shows data about the temperatures and precipitation for the season, and the playbook from the City of Austin which has information about heat resilience and strategies to curb the heat. The Co-Lab has also been interviewed by KXAN last week and some insight has been shared on what we do at the lab! We had Allysa, Alexia, and Manmeet talk about the work that they do and how UT Austin works in collaboration with the City of Austin to help fight climate change!",
    image: "https://content.money.com/wp-content/uploads/2019/08/28-university-of-texas-at-austin_36889b-1.jpg?quality=60", // Replace with the actual image URL or import statement
    link: "/news/article2", // Replace with the actual route you want to link to
  },

  {
    title: "Paris 2024 Olympics RDP",
     date: "July 26th 2024",
     content: "A major international project supported by WMO is using the Paris 2024 Olympics as an opportunity to advance weather forecasting research, and researchers at The University of Texas at Austin are leading the charge!The team takes the NCEP Global Forecast System (GFS) and integrates it with Google Graphcast that has been trained for global meteorological output at 25km grid spacing every 6 hour average- winds, temperature, and precipitation for the NOAA Analysis of Record for Calibration AORC. We have Manmeet Singh Naveen Sudharsan leading the overall effort in close partnership with Texas Advanced Computing Center (TACC), followed by Naveen and Arya Chavoshi developing the downscaling, followed Harsh Kamath and Trevor Brooks looking into the thermal comfort product, and passing it on to Aditya Patel and Parika Grover for developing the web interface and updates, followed by Sasanka Talukdar and Allysa Dallmann developing the daily weather summary.",
     image: "https://wmo.int/sites/default/files/styles/featured_image_x2_1536x1024_/public/2024-07/Paris%20Olympics%202024.jpg?h=2aa300aa&itok=7U138NYm", // Replace with the actual image URL or import statement
     link: "/news/article2", // Replace with the actual route you want to link to
   },

   {
    title: "Global Urban Precipitation Anomaly",
     date: "September 09th 2024",
     content: "Urbanization has accelerated dramatically across the world over the past decades. Urban influence on surface temperatures is now being considered as a correction term in climatological datasets. Although prior research has investigated urban influences on precipitation for specific cities or selected thunderstorm cases, no study to date has comprehensively revealed the urban precipitation anomalies on a global scale. This research is the first global analysis of urban precipitation anomalies for over one thousand cities worldwide. We find that more than 60% of the global cities and their downwind regions are receiving more precipitation than the surrounding rural areas. Moreover, the magnitude of these urban wet islands has nearly doubled in the past 20 years. Urban precipitation anomalies exhibit variations across different continents and climates, with cities in Africa, for example, exhibiting the largest urban annual and extreme precipitation anomalies. Cities are more prone to substantial urban precipitation anomalies under warm and humid climates compared to cold and dry climates. Cities with larger populations, pronounced urban heat island effects, and higher aerosol loads also show noticeable precipitation enhancements. This research maps global urban rainfall hotspots, establishing a foundation for the consideration of urban rainfall corrections in climatology datasets. This advancement holds promise for projecting extreme precipitation and fostering the development of more resilient cities in the future.",
     image: "/images/CloudyUT.jpg", // Replace with the actual image URL or import statement
     link: "/news/article2", // Replace with the actual route you want to link to
   },

   {
    title: "UT-City Climate CoLab 2025 Winter Outlook",
     date: "Jan 03th 2025",
     content: "We're excited to share the UT-City Climate CoLab 2025 Winter Outlook! This winter outlook dives into the latest climate projections, offering valuable insights to help policymakers, community leaders, and organizations prepare for the unique challenges the upcoming winter season may bring to Austin. As climate variability continues to pose both challenges and opportunities for adaptation, the UT-City Climate CoLab’s Winter Outlook is an essential resource for those responsible for planning and decision-making. The 2025 Winter Outlook emphasizes the importance of preparedness, urging stakeholders to stay proactive and collaborative in addressing the risks associated with the changing climate.",
     image: "/images/winteroutlook.jpeg", // Replace with the actual image URL or import statement
     link: "/news/article2", // Replace with the actual route you want to link to
   },

   {
    title: "Attended American Meteorological Society 2025",
     date: "Jan 15th 2025",
     content: "The Extreme Weather and Urban Sustainability Lab (TExUS Lab) is excited to have attended the American Meteorological Society 2025 annual meeting in New Orleans. We had speakers present various topics, ranging from AI, UrbanRainfall, ExtremePrecipitation, AppliedClimate, UTCityClimateCoLab, and much more!",
     image: "/images/AMS.jpeg", // Replace with the actual image URL or import statement
     link: "/news/article2", // Replace with the actual route you want to link to
   },

  // Add more news articles as needed
];

export default NewsInfo;
