import React, { useState, useEffect }from 'react';
import './ParisRDP.css'; // Create a new CSS file for Paris styling.
import { Link } from 'react-router-dom';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import 'lightbox2/dist/css/lightbox.min.css';
import lightbox from 'lightbox2';


function Paris() {
  const [value, onChange] = useState(new Date());
  useEffect(() => {
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true,
      'fadeDuration': 600,
      'imageFadeDuration': 600,
    });
  }, []);
  
  const animationresponsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 480, min: 0 },
      items: 1
    }
  };

  const onChangeDate = (date) => {
    const checkDate = new Date(date);
    const stringDate = checkDate.toString();

    if(stringDate.includes("Jul 26") == true){
      //archiveurl
      window.open('/ParisOlympics26Jul', '_blank');
    }

    if(stringDate.includes("Jul 29") == true){
      //archiveurl
      window.open('/ParisOlympics29Jul', '_blank');
    }
    if(stringDate.includes("Jul 30") == true){
      //archiveurl
      window.open('/ParisOlympics30Jul', '_blank');
    }
    if(stringDate.includes("Jul 31") == true){
      //archiveurl
      window.open('/ParisOlympics31Jul', '_blank');
    }
    if(stringDate.includes("Aug 01") == true){
      //archiveurl
      window.open('/ParisOlympics01Aug', '_blank');
    }
    if(stringDate.includes("Aug 02") == true){
      //archiveurl
      window.open('/ParisOlympics02Aug', '_blank');
    }
    if(stringDate.includes("Aug 03") == true){
      //archiveurl
      window.open('/ParisOlympics03Aug', '_blank');
  }
  if(stringDate.includes("Aug 04") == true){
    //archiveurl
    window.open('/ParisOlympics04Aug', '_blank');
}
if(stringDate.includes("Aug 05") == true){
  //archiveurl
  window.open('/ParisOlympics05Aug', '_blank');
}
if(stringDate.includes("Aug 06") == true){
  //archiveurl
  window.open('/ParisOlympics06Aug', '_blank');
}
if(stringDate.includes("Aug 07") == true){
  //archiveurl
  window.open('/ParisOlympics07Aug', '_blank');
}
if(stringDate.includes("Aug 08") == true){
  //archiveurl
  window.open('/ParisOlympics08Aug', '_blank');
}
if(stringDate.includes("Aug 09") == true){
  //archiveurl
  window.open('/ParisOlympics09Aug', '_blank');
}
if(stringDate.includes("Aug 10") == true){
  //archiveurl
  window.open('/ParisOlympics10Aug', '_blank');
}
if(stringDate.includes("Aug 11") == true){
  //archiveurl
  window.open('/ParisOlympics11Aug', '_blank');
}
if(stringDate.includes("Aug 28") == true){
  //archiveurl
  window.open('/ParisOlympics28Aug', '_blank');
}
  };

  const tileClassName = ({ date, view }) => {
    // List of dates that should be highlighted
    const highlightedDates = [
        "Jul 26", "Jul 29", "Jul 30", "Jul 31", "Aug 01", "Aug 02", "Aug 03", "Aug 04",
        "Aug 05", "Aug 06", "Aug 07", "Aug 08", "Aug 09", "Aug 10", "Aug 11", "Aug 28","Sep 02"
    ];

    const dateString = date.toDateString();

    // Check if the current date matches any of the highlighted dates
    if (highlightedDates.some(highlightedDate => dateString.includes(highlightedDate))) {
        return 'highlighted-date'; // Class that will apply to the highlighted date
    }
};

  return (
    <div className="main">
 <div className="parisheader">
    <img src="images/parisbackround.png" alt="Background" className="paris-background" />

    <div className="header-container">
        <div className="title-content">
            <div className="title-logo">
              <Link to="/" >
                <button className="back-button">
                  <i className="fa fa-arrow-left"></i> Back to Home
                </button>
              </Link>
                <img src="images/parislogo.png" alt="Paris Olympic logo" />
                <h1>Paris Olympics RDP</h1>
            </div>
            <div className="heading-text">
                <h2>UT Meteogan Downscaling in partnership with Texas Advanced Computing Center (TACC) as part of World Meteorological Organization (WMO) Remote Desktop Protocol (RDP)</h2><br></br>
                <div className="button-row">
                    <a href="https://www.linkedin.com/posts/research-at-the-university-of-texas-at-austin_olympics-meteorological-paris2024-activity-7222594602212454401-mCF-?utm_source=share&utm_medium=member_android" className="social-button">
                    <img src="/images/linkedin-icon.avif" alt="LinkedIn" className="icon" /> 
                        <span className="underline-text">Read UT Research Announcement</span>
                        <i class="fa-solid fa-arrow-up-right-from-square" style={{color: '#ffffff', marginLeft:'10px'}}></i>
                    </a>
                    <a href="https://wmo.int/media/news/paris-olympics-will-advance-research-weather-forecasting-and-urban-meteorology" target="_blank" rel="noopener noreferrer" className="social-button">
                        <img src="/images/wmo-icon.jpg" alt="External Link" className="icon" /> 
                        <span className="underline-text">Read WMO Article</span>
                        <i class="fa-solid fa-arrow-up-right-from-square" style={{color: '#ffffff', marginLeft:'10px'}}></i>
                    </a>
                </div>
            </div>
        </div>

        <div className="calendargrid">
            <h1 style={{fontSize: '1.5rem',textAlign:'center', paddingBottom:'10px'}}>Forecast Calendar</h1>
            <Calendar
                onChange={onChangeDate}
                value={value}
                minDate={new Date('July 26, 2024 00:00:00')}
                maxDate={new Date('September 2, 2024 23:59:59')}
                tileClassName={tileClassName}             
            />
        </div>
    </div>

    <div className="footer-content">
        <p><strong>Experimental Forecasts from UT Austin - Jackson School of Geosciences/ TExUS Lab</strong></p>
        <p>This is an experimental/research model study. The model runs are on TACC compute resources at UT. This experimental forecast is not for public advisory or use, or any decision making and is only updated here for the lab and project teams to test the models. No liability is assumed. The official forecasts are available from NOAA/NWS <a href="https://www.weather.gov/ewx/" target="_blank" rel="noopener noreferrer">https://www.weather.gov/ewx/</a> and <a href="https://www.nhc.noaa.gov/" target="_blank" rel="noopener noreferrer">https://www.nhc.noaa.gov/</a>.</p>
        <br></br><p><a href="https://wmo.int/media/news/paris-olympics-will-advance-research-weather-forecasting-and-urban-meteorology" target="_blank" rel="noopener noreferrer">Image Source: WMO News</a></p>
    </div>
</div>
     
      <div class="graph-title">
        <h1>Observed Precipitation from Radar</h1>
        </div>
        <Carousel responsive={animationresponsive}>
      <div class="graph-box">
        <h2>1 Aug 2024</h2>
        <p>02:15-04:15</p>
        <div class="video-container">
            <video width="300px" height="400px" controls="controls">
                <source src="/videos/Forecastrecording_1aug_2-4.mov" />
            </video>
        </div>
      </div>
      <div class="graph-box">
      <h2>1 Aug 2024</h2>
      <p>06:20-08:20</p>
        <div class="video-container">
            <video width="300px" height="400px" controls="controls">
              <source src="/videos/Forecastrecording_1aug_6-8.mov" />
            </video>
        </div>
      </div>
      <div class="graph-box">
      <h2>2 Aug 2024</h2>
      <p>14:00-16:00</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_2aug_14-16.mov" />
    </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>2 Aug 2024</h2>
      <p>18:20-20:20</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_2aug_18-20.mov" />
     </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>2 Aug 2024</h2>
      <p>22:30-00:30</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_2aug_22.mov" />
    </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>2 Aug 2024</h2>
      <p>01:55-03:50</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_2aug_1-3.mov" />
    </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>3 Aug 2024</h2>
      <p>07:10-09:10</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_3aug_7-9.mov" />
    </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>3 Aug 2024</h2>
      <p>14:30-14:30</p>
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_3aug_14-16.mov" />
    </video>
      </div>
      <div class="graph-box">
      <h2>4 Aug 2024</h2>
      <p>03:45-05:45</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_4aug_3-6.mov" />
    </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>5 Aug 2024</h2>
      <p>23:15-01:15</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_6aug_23-1.mov" />
    </video>
      </div>
      </div>
      <div class="graph-box">
      <h2>6 Aug 2024</h2>
      <p>23:45-01:45</p>
      <div class="video-container">
      <video width="300px" height="400px" controls="controls">
        <source src="/videos/Forecastrecording_6aug_23_2.mov" />
    </video>
      </div>
      </div>
      </Carousel>

      <div class="border-line"></div>
     
      <div class="forecast-title">
        <h1>Model Forecast starting September 2 2024 00 UTC</h1>
        <h2>Precipitation Forecast</h2>
        <p>Paris is forecasted to have another dry day today, with most of the rainfall confined to areas west of the city. Rain is expected to develop over the English Channel in the evening but will likely dissipate before reaching Paris. Tomorrow morning, light showers are anticipated in the southeastern part of the city.</p>
        </div>
      
      <div className="graphs-container">

        <div className="image-wrapper">
          <a href="/images/mean_surf_precp_2sep.png" data-lightbox="forecast-gallery" data-title="Mean Surface Precipitation at 2024-09-03">
            <img src="/images/mean_surf_precp_2sep.png" alt="Precipitation Forecast" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Mean Surface Precipitation at 2024-09-03 UTC</p>
            </div>
          </a>
        </div>
        
        {/* Image 2 */}
        <div className="image-wrapper">
          <a href="/images/total_surf_precp_2sep_24hrs.png" data-lightbox="forecast-gallery" data-title="Total Surface Precipitation for the next 24 Hours starting 2 Sep 2024 00 UTC" >
            <img src="/images/total_surf_precp_2sep_24hrs.png" alt="24 Hours Precipitation" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Total Surface Precipitation for the next 24 Hours starting 2 Sep 2024 00 UTC</p>
            </div>
          </a>
        </div>

        {/* Image 3 */}
        <div className="image-wrapper">
          <a href="/images/total_surf_precp_2sep_36hrs.png" data-lightbox="forecast-gallery" data-title="Total Surface Precipitation for the next 36 Hours starting 2 Sep 2024 00 UTC" >
            <img src="/images/total_surf_precp_2sep_36hrs.png" alt="36 Hours Precipitation" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Total Surface Precipitation for the next 36 Hours starting 2 Sep 2024 00 UTC</p>
            </div>
          </a>
        </div>

        {/* Image 4 */}
        <div className="image-wrapper">
          <a href="/images/total_surf_prec_2sep.gif" data-lightbox="forecast-gallery" data-title="Total Surface Precipitation at 2024-09-03" >
            <img src="/images/total_surf_prec_2sep.gif" alt="Animated Precipitation" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Total Surface Precipitation at 2024-09-03 UTC</p>
            </div>
          </a>
        </div>
      </div>

      <div class="forecast-title">
      <h2>Surface Temperature Forecast</h2>
      <p>Temperatures in Paris today are expected to remain mild, with a high of around 24°C in the evening. The average temperature will be about 22°C, gradually cooling to 17°C by early morning.</p>
      </div>

      <div className="graphs-container">
        {/* Image 1 */}
        <div className="image-wrapper">
          <a href="/images/mean_surf_air_temp_2sep.png" data-lightbox="forecast-gallery" data-title="Mean Surface Air Temperature at 2024-09-03 UTC" >
            <img src="/images/mean_surf_air_temp_2sep.png" alt="Mean Surface Air Temperature" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Mean Surface Air Temperature at 2024-09-03 UTC</p>
            </div>
          </a>
        </div>

        {/* Image 2 */}
        <div className="image-wrapper">
          <a href="/images/avg_surf_air_temp_2sep_24hrs.png" data-lightbox="forecast-gallery" data-title="Average Surface Air Temperature for the next 24 Hours starting 2 Sep 2024 00 UTC" >
            <img src="/images/avg_surf_air_temp_2sep_24hrs.png" alt="Average Surface Air Temperature 24 hrs" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Average Surface Air Temperature for the next 24 Hours starting 2 Sep 2024 00 UTC</p>
            </div>
          </a>
        </div>

          {/* Image 3 */}
          <div className="image-wrapper">
          <a href="/images/avg_surf_air_temp_2sep_36hrs.png" data-lightbox="forecast-gallery" data-title="Average Surface Air Temperature for the next 36 Hours starting 2 Sep 2024 00 UTC" >
            <img src="/images/avg_surf_air_temp_2sep_36hrs.png" alt="Average Surface Air Temperature 36 hrs" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Average Surface Air Temperature for the next 36 Hours starting 2 Sep 2024 00 UTC</p>
            </div>
          </a>
        </div>

        {/* Image 4 */}
        <div className="image-wrapper">
          <a href="/images/surfTemp_srp2.gif" data-lightbox="forecast-gallery" data-title="Surface Temperature at 2024-09-03 UTC" >
            <img src="/images/surfTemp_srp2.gif" alt="Surface Temperature" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Surface Temperature at 2024-09-03 UTC</p>
            </div>
          </a>
        </div>
      </div>

      <div class="forecast-title">
      <h2>Wind Forecast</h2>
      <p>Winds are expected to stay mild, with maximum surface speeds of around 12 km/h from the southwest. The winds are primarily driven by a low-pressure area over the English Channel.</p>
      </div>
      <div className="graphs-container">

         {/* Image 1 */}
         <div className="image-wrapper">
          <a href="/images/mean_surf_winds_2sep.png" data-lightbox="forecast-gallery" data-title="Mean Surface Winds at 2024-09-03 UTC" >
            <img src="/images/mean_surf_winds_2sep.png" alt="Mean Surface Winds" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Mean Surface Winds at 2024-09-03 UTC</p>
            </div>
          </a>
        </div>

         {/* Image 2 */}
         <div className="image-wrapper">
          <a href="/images/mean_winds_2sep_24hrs.png" data-lightbox="forecast-gallery" data-title="Mean Surface Winds for the next 24 Hours starting 2 Sep 2024 00 UTC" >
            <img src="/images/mean_winds_2sep_24hrs.png" alt="Mean Surface Winds 24 hours" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Mean Surface Winds for the next 24 Hours starting 2 Sep 2024 00 UTC</p>
            </div>
          </a>
        </div>

        {/* Image 3 */}
        <div className="image-wrapper">
          <a href="/images/mean_winds_2sep_36hrs.png" data-lightbox="forecast-gallery" data-title="Mean Surface Winds for the next 36 Hours starting 2 Sep 2024 00 UTC" >
            <img src="/images/mean_winds_2sep_36hrs.png"  alt="Mean Surface Winds 36 hours" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Mean Surface Winds for the next 36 Hours starting 2 Sep 2024 00 UTC</p>
            </div>
          </a>
        </div>

          {/* Image 4 */}
          <div className="image-wrapper">
          <a href="/images/winds_sep2.gif" data-lightbox="forecast-gallery" data-title="Winds at 2024-09-03 UTC" >
            <img src="/images/winds_sep2.gif"  alt="Winds at 2024-09-03 UTC" className="forecast-image" loading="eager" />
            <div className="forecastimage-overlay">
              <p>Winds at 2024-09-03 UTC</p>
            </div>
          </a>
        </div>
      </div>

      <div className="meteoganimage-container">
      <h1>UT-MeteoGAN details</h1>
        <p>Click below to view the MeteoGan Schematic</p>
        <a href="images/ut_meteogan_schematic.png"> <button className="parisbutton">MeteoGAN Schematic</button></a>
      </div>
 

      <div class="thermscale-box">
      <h1>Demo: Universal Thermal Climate Index for Paris</h1>
      <p> <bold>6 August 2024, 11 AM</bold></p>
      <br/>
      <iframe src="Paris_thermscale.html" width="100%" height="720"></iframe>
      </div>

      <div className="thermalscape-vid">
        <h1>Video of Universal Thermal Climate Index along Olympic marathon route </h1>
        <video loop autoPlay playsInline>
        <source src={process.env.PUBLIC_URL + '/videos/marathon-vid.mp4'} />
        </video>
      </div>

      <div class="thermscale-box">
        <h1>Demo: UTCI Along The Olympics Marathon Path</h1><br/>
      <iframe width="100%" height="720" frameborder="0" scrolling="no" allowfullscreen src="https://ut-austin.maps.arcgis.com/apps/instant/3dviewer/index.html?appid=0624bfad92da47ffaa3e74b5689aacbc"></iframe>

      </div>

      <div className="thermalscape-img">
        <h1>UTCI plotted for running along the marathon track </h1>
        <img src="/images/graph-marathon.png"/>
      </div>
      
    <footer>
        <p>&copy; 2024 University of Texas at Austin, Jackson School of Geosciences. All rights reserved.</p>
    </footer>
    </div>
  );
}

export default Paris;
