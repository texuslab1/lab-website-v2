import React, { useEffect, useState } from 'react';
import Papa from 'papaparse';

function CSVReader({ csvFilePath }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch(csvFilePath);
        const text = await response.text();
        const result = Papa.parse(text, { header: true, skipEmptyLines: true });
        setData(result.data);
      } catch (error) {
        console.error('Error reading CSV:', error);
      }
    }

    fetchData();
  }, [csvFilePath]);

  return (
    <div>
      <h2>CSV Data</h2>
      <table>
        <thead>
          <tr>
            {data.length > 0 &&
              Object.keys(data[0]).map((key) => <th key={key}>{key}</th>)}
          </tr>
        </thead>
        <tbody>
          {data.map((row, index) => (
            <tr key={index}>
              {Object.values(row).map((value, index) => (
                <td key={index}>{value}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default CSVReader;