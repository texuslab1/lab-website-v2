import React from 'react';
import './Content.css';

function Content({ title, pictureSrc, content, targetSection }) {
  return (
    <div className="content-section" id={targetSection}>
      <div className="content-wrapper">
        <div className="content-image">
          <div className="image-container">
            <img src={pictureSrc} alt={title} />
          </div>
        </div>
        <div className="content-text">
          <h2>{title}</h2>
          <p>{content}</p>
        </div>
      </div>
    </div>
  );
}

export default Content;