import React, { useState, useEffect } from 'react';
import './VideoCarousel.css';

const VideoCarousel = ({ videoSources }) => { // Pass videoSources as a prop
  const [currentVideoIndex, setCurrentVideoIndex] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentVideoIndex((currentVideoIndex + 1) % videoSources.length);
    }, 5000); // Change video every 10 seconds

    return () => {
      clearInterval(interval);
    };
  }, [currentVideoIndex, videoSources]);

  const prevVideo = () => {
    setCurrentVideoIndex((currentVideoIndex - 1 + videoSources.length) % videoSources.length);
  };

  const nextVideo = () => {
    setCurrentVideoIndex((currentVideoIndex + 1) % videoSources.length);
  };

  return (
    <div className="carousel-container">
      <button className="prev-button" onClick={prevVideo}>
        &#10094;
      </button>
      <video className="carousel-video" autoPlay muted loop>
        <source src={videoSources[currentVideoIndex]} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
      <button className="next-button" onClick={nextVideo}>
        &#10095;
      </button>
    </div>
  );
};

export default VideoCarousel;