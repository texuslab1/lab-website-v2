const newsData = [

    {
    month: 'OCT',
    day: '5',
    content: 'Dr. Atul Jain, Professor, University of Illinois visits UT Austin',
  },

  {
    month: 'OCT',
    day: '9',
    content: 'Urban Climate Solutions Workshop on the campus of Texas A&M University, College Station, TX',
    linkTo: "/news-article"
  },

  {
    month: 'OCT',
    day: '14',
    content: 'The TExUS team traveled to Boerne, TX to view the 2023 Solar Eclipse',
  },
  {
    month: 'NOV',
    day: '9',
    content: 'Harsh successfully completes his Qualifying Exam!',

  },
    {
    month: 'Jan',
    day: '16',
    content: 'New class: GeoHealth',

  },
    {
    month: 'Jan',
    day: '28',
    content: 'The group visits AMS!',

  },
    {
    month: 'Feb',
    day: '8',
    content: 'First In Person CoLab Workshop',

  }




  
  // Add more news items as needed
];

export default newsData;